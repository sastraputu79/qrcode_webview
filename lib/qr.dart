import 'package:flutter/material.dart';
import 'package:project_scan_qr/url.dart';
import 'package:qrscan/qrscan.dart' as scanner;

// ignore: camel_case_types
class qrcode extends StatefulWidget {
  @override
  _qrcodeState createState() => _qrcodeState();
}

// ignore: camel_case_types
class _qrcodeState extends State<qrcode> {
  String text = "Klik Pindai untuk Memindai Kode QR!";
  Color _setColor = Colors.blue;
  // String btn = "Scan";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
            child: Column(
      // mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SizedBox(
          height: 50,
        ),
        Container(
          child: Column(
            children: [
              Text(
                "APLIKASI PEMINDAI KODE QR",
                style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
              ),
              Image.asset('assets/images/splash_screen.jpg'),
            ],
          ),
        ),
        SizedBox(
          height: 80,
        ),
        TextButton(
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => UrlResult(text: text)));
            },
            child: Text(
              text,
              style: TextStyle(fontSize: 20, color: _setColor),
              textAlign: TextAlign.center,
            )),
        SizedBox(height: 25),
        // ignore: deprecated_member_use
        RaisedButton(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(80),
          ),
          onPressed: () async {
            text = await scanner.scan();
            _setColor = Colors.green;
            setState(() {});
          },
          child: Container(
            width: 100,
            height: 40,
            alignment: Alignment.center,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(Icons.qr_code_scanner_sharp),
                SizedBox(width: 5),
                Text(
                  "Pindai",
                  style: TextStyle(fontSize: 18),
                ),
              ],
            ),
          ),
        ),
        SizedBox(
          height: 100,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text("Copyright @"),
            Text(
              " Sastra Krisna Yana ",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ],
        )
      ],
    )));
  }
}
